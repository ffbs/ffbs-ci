import os
import re
import subprocess
import time

import pytest
import requests

from conftest import ipv6_link_local


def assert_web(url, text_to_find):
    r = requests.get(url)
    assert r.status_code == 200, "Trying to get url {} failed with status code {}. Was looking for text '{}' there...".format(url, r.status_code, text_to_find)
    assert text_to_find in r.text, "Could not find '{}' in {}. Site returned:\n{}".format(text_to_find, url, r.text)


def assert_not_web(url, text_to_find):
    r = requests.get(url)
    assert r.status_code == 200, "Trying to get url {} failed with status code {}. Was looking for text '{}' there...".format(url, r.status_code, text_to_find)
    assert not text_to_find in r.text, "Could find '{}' in {}. Site returned:\n{}".format(text_to_find, url, r.text)

def test_user_has_id_rsa():
    ssh_dir = os.path.join(
            os.environ['HOME'],
            ".ssh",
            )
    assert os.path.isfile(os.path.join(ssh_dir, "id_rsa")), "User has no ~/.ssh/id_rsa"
    assert os.path.isfile(os.path.join(ssh_dir, "id_rsa.pub")), "User has no ~/.ssh/id_rsa.pub"

@pytest.mark.lg_feature("uboot-interactive")
def test_uboot(target, in_uboot):
    command = target.get_driver('UBootDriver')

    stdout, stderr, returncode = command.run('version')
    assert returncode == 0
    assert len(stdout) > 0
    assert len(stderr) == 0
    assert 'U-Boot' in '\n'.join(stdout)

@pytest.mark.lg_feature("uboot-interactive")
def test_help(target, in_uboot):
    command = target.get_driver('UBootDriver')

    stdout, stderr, returncode = command.run('help')
    assert returncode == 0
    assert len(stdout) > 0
    assert len(stderr) == 0
    assert 'boot' in '\n'.join(stdout)

@pytest.mark.config_mode
def test_transition_to_config_mode(in_good_config):
    pass

@pytest.mark.config_mode
def test_good_config_dhcp(target, host_container):
    for _ in range(5):
        stdout, _, _ = host_container.run("ip -br a")
        if "192.168.1." in " ".join(stdout):
            break
        time.sleep(10)
    else:
        assert False, "No IP via DHCP received in time"

@pytest.mark.config_mode
def test_good_config_default_coordinates(target, in_good_config, config):
    assert_web("http://192.168.1.1/cgi-bin/config/wizard", config["location_lat"])
    assert_web("http://192.168.1.1/cgi-bin/config/wizard", config["location_lon"])

@pytest.mark.config_mode
@pytest.mark.flaky(reruns=5, reruns_delay=10)
def test_good_config_altitude_field(target, in_good_config):
    assert_web("http://192.168.1.1/cgi-bin/config/wizard", "id.1.6.altitude")

@pytest.mark.config_mode
def test_good_config_no_contact(target, in_good_config):
    assert_not_web("http://192.168.1.1/cgi-bin/config/wizard", "id.1.6.contact")

@pytest.mark.config_mode
def test_good_config_autoupdater_enabled(target, in_good_config):
    assert_web("http://192.168.1.1/cgi-bin/config/admin/autoupdater", 'value="1" id="id.1.1.enabled" name="id.1.1.enabled" checked="checked"')

@pytest.mark.config_mode
def test_ssh_authorized_keys(target, in_good_config, command):
    r = requests.get('http://192.168.1.1/cgi-bin/config/admin/remote')
    match = re.search('<textarea[^>]+>(.*)</textarea>', r.text, re.DOTALL)
    assert match, 'Could not fetch authorized keys'
    keys = match[1].strip()
    stdout, _, code = command.run('cat /lib/gluon/release')
    assert code == 0, 'Error getting firmware version'
    version = ''.join(stdout).strip()
    if version.endswith('beta'):
        assert 'ssh' in keys, 'Found beta version but no keys are authorized'
    elif version.endswith('stable'):
        assert len(keys) == 0, 'Found stable but at least one key is authorized'
    else:
        return pytest.skip('Version is neither beta nor stable')

@pytest.mark.config_mode
def test_info_pull_auth_keys_file(command):
    auth_keys_file = "/usr/share/info-pull/.ssh/authorized_keys"
    _, _, rc = command.run(f"ls {auth_keys_file}")
    assert rc == 0, "info_pull authorized_keys file not found"

@pytest.mark.config_mode
def test_info_pull_auth_keys_file(command):
    auth_keys_file = "/usr/share/info-pull/.ssh/authorized_keys"
    keys_file, _, rc = command.run(f"cat {auth_keys_file}")
    assert rc == 0, "info_pull authorized_keys file not found"

    for line in keys_file:
        assert line.startswith("command=")

@pytest.mark.running_mode
def test_transition_to_running_mode(in_good_running):
    pass

@pytest.mark.running_mode
@pytest.mark.flaky(reruns=10, reruns_delay=10)
def test_good_running_ssh(target, in_good_running, config, command):
    ip = ipv6_link_local(command)
    ifname = target.get_resource("RemoteNetworkInterface").ifname
    keyfile= os.path.join(
            os.environ['HOME'],
            ".ssh",
            "id_rsa",
            )
    r = subprocess.run([
        "ssh", 
        "-o", "UserKnownHostsFile=/dev/null", 
        "-o", "StrictHostKeyChecking=no", 
        "-o", "PreferredAuthentications=publickey", 
        "-i", keyfile,
        'root@{}%{}'.format(ip, ifname), 
        "echo 1"
        ])
    assert r.returncode == 0, "SSH connection during run mode failed"

@pytest.mark.running_mode
@pytest.mark.parametrize("uci_key,uci_value,should_be_set", [
    ("wireless.client_radio0.ssid", "Freifunk", True),
    ("wireless.client_radio0.disabled", "0", True),
    ("wireless.client_radio0.mode", "ap", True),

    ("wireless.mesh_radio0.disabled", "0", True),
    ("wireless.mesh_radio0.mesh_id", "eseB6Qlu", True),
    ("wireless.mesh_radio0.mode", "mesh", True),

    ("wireless.radio0.legacy_rates", "0", True),
] )
def test_good_running_uci_config(target, in_good_running, uci_key, uci_value, should_be_set, command):

    stdout, _, _ = command.run("uci show {}".format(uci_key))

    key_value_pairs = []
    test_key = None
    test_value = None

    for line in stdout:
        kv = line.split('=')
        if kv[0] == uci_key:
            test_key = kv[0]
            test_value = kv[1].replace("'","").replace('"','')


    assert not stdout == "uci: Entry not found", "UCI does not contain entry: {}".format(uci_key)
    if should_be_set:
        assert test_key == uci_key, "UCI Key {} probably not found in output: {}".format(uci_key, stdout)
        assert test_value == uci_value, "UCI Key {} does not contain value {} but returned {} instead".format(uci_key, uci_value, stdout)
    else:
        assert test_value != uci_value, "UCI field {} is set to value {}".format(uci_key, uci_value)

@pytest.mark.running_mode
@pytest.mark.flaky(reruns=10, reruns_delay=10)
def test_good_running_private_wifi_ssid(target, in_good_running, config, command):
    stdout, _, _ = command.run("iw dev")
    assert config["private_wifi_ssid"] in " ".join(stdout), "Did not find any wifi-interface with SSID {}".format(config["private_wifi_ssid"])

@pytest.mark.running_mode
def test_good_running_private_wifi_psk(target, in_good_running, config, command):
    stdout, _, _ = command.run("uci show wireless")
    assert config["private_wifi_psk"] in " ".join(stdout), "Did not find any wifi-interface with PSK {}".format(config["private_wifi_psk"])

@pytest.mark.running_mode
def test_generated_wg_key(target, in_good_running, command):
    stdout, _, code = command.run('cat /etc/parker/wg-privkey | wg pubkey')
    assert code == 0, "Verification of WG private key failed: {}".format(''.join(stdout))

@pytest.mark.running_mode
@pytest.mark.flaky(reruns=10, reruns_delay=10)
def test_good_running_info_pull_no_key(target, in_good_running, command):
    ip = ipv6_link_local(command)
    ifname = target.get_resource("RemoteNetworkInterface").ifname
    keyfile= os.path.join(
            os.environ['HOME'],
            ".ssh",
            "id_rsa",
            )
    r = subprocess.run([
        "ssh",
        "-o", "UserKnownHostsFile=/dev/null",
        "-o", "StrictHostKeyChecking=no",
        "-o", "PreferredAuthentications=publickey",
        "-i", keyfile,
        'info-pull@{}%{}'.format(ip, ifname),
        ])
    assert r.returncode == 255, "SSH to info_pull without key worked!"

@pytest.mark.running_mode
@pytest.mark.flaky(reruns=10, reruns_delay=10)
def test_good_running_info_pull_with_key(target, in_good_running, command, info_pull_ssh_key):
    # generate a info file even if the cronjob has not run yet
    _, _, rc = command.run("/usr/share/info-pull/info-export.sh")

    ip = ipv6_link_local(command)
    ifname = target.get_resource("RemoteNetworkInterface").ifname
    keyfile= os.path.join(
            os.environ['HOME'],
            ".ssh",
            "id_rsa",
            )
    r = subprocess.run([
        "ssh",
        "-o", "UserKnownHostsFile=/dev/null",
        "-o", "StrictHostKeyChecking=no",
        "-o", "PreferredAuthentications=publickey",
        "-i", keyfile,
        'info-pull@{}%{}'.format(ip, ifname),
        ],
        check=True,
        stdout=subprocess.PIPE,
    )

    assert r.returncode == 0
    assert "ffci-1" in r.stdout.decode()

@pytest.mark.running_mode
@pytest.mark.flaky(reruns=10, reruns_delay=10)
def test_good_running_info_pull_command_fencing(target, in_good_running, command, info_pull_ssh_key):
    ip = ipv6_link_local(command)
    ifname = target.get_resource("RemoteNetworkInterface").ifname
    keyfile= os.path.join(
            os.environ['HOME'],
            ".ssh",
            "id_rsa",
            )
    r = subprocess.run([
        "ssh",
        "-o", "UserKnownHostsFile=/dev/null",
        "-o", "StrictHostKeyChecking=no",
        "-o", "PreferredAuthentications=publickey",
        "-i", keyfile,
        'info-pull@{}%{}'.format(ip, ifname),
        "echo", "freifunkIstCool",
        ],
        check=True,
        stdout=subprocess.PIPE,
    )

    assert r.returncode == 0
    assert "freifunkIstCool" not in r.stdout.decode()

@pytest.mark.running_mode
def test_good_running_info_pull_non_legacy(target, in_good_running, command):

    stdout, _, rc = command.run("uci show gluon-node-info")
    assert rc == 0

    for line in stdout:
        assert ".contact=" not in line

@pytest.mark.running_mode
def test_good_running_info_share_default(target, in_good_running, command):

    stdout, _, rc = command.run("uci get parker.owner.publish")
    assert rc == 0

    assert "".join(stdout) == "0"

@pytest.mark.running_mode
@pytest.mark.flaky(reruns=20, reruns_delay=10)
def test_good_running_wan_dns(target, in_good_running, config, command):
    stdout, _, _ = command.run(
        "LIBPACKETMARK_MARK=1 LD_PRELOAD=libpacketmark.so ping -4 -c 1 gandolf.stratum0.org"
    )
    assert config["dns_gandolf_4"] in " ".join(stdout)

@pytest.mark.running_mode
@pytest.mark.flaky(reruns=20, reruns_delay=10)
def test_good_running_wan_ping_confighost(target, in_good_running, config, command):
    stdout, _, _ = command.run(
        "LIBPACKETMARK_MARK=1 LD_PRELOAD=libpacketmark.so ping -4 -c 10 config.freifunk-bs.de"
    )
    for l in stdout:
        if "transmitted" in l:
            match = re.search(r"(\d+) packets received", l, re.DOTALL)
            assert match, "Ping result unparseable"
            assert int(match[1]) > 5, "Packetloss is > 50%"
            break
    else:
        assert False, "Ping failed!"

@pytest.mark.running_mode
@pytest.mark.flaky(reruns=20, reruns_delay=10)
def test_good_running_wan_curl_confighost(target, in_good_running, config, command):
    stdout, _, rc = command.run(
        "LIBPACKETMARK_MARK=1 LD_PRELOAD=libpacketmark.so wget http://config.freifunk-bs.de/config"
    )
    assert rc in [
        0, # Status 200
        8, # Status 400
    ], "could not reach config-server via HTTP"

@pytest.mark.running_mode
@pytest.mark.flaky(reruns=20, reruns_delay=10)
def test_good_running_count_wg_ifs(target, in_good_running, config, command):
    stdout, _, rc = command.run(
        "ip link"
        )
    count = 0
    for l in stdout:
        match = re.search(r"\d+: (wg_c\d):", l, re.DOTALL)
        if match:
            count += 1
    assert count > 0, "No wireguard interfaces available!"

@pytest.mark.running_mode
@pytest.mark.flaky(reruns=20, reruns_delay=10)
def test_good_running_ipv4_default(target, in_good_running, config, command):
    stdout, _, rc = command.run(
        "ip -4 route"
        )
    for l in stdout:
        if "default" in l:
            if "dev wg_c" in l:
                break
    else:
        assert False, "No Default route via wireguard found!"

@pytest.mark.running_mode
@pytest.mark.flaky(reruns=20, reruns_delay=10)
def test_good_running_ipv6_default(target, in_good_running, config, command):
    stdout, _, rc = command.run(
        "ip -6 route"
        )
    for l in stdout:
        if "default" in l:
            if "dev wg_c" in l:
                break
    else:
        assert False, "No Default route via wireguard found!"

@pytest.mark.running_mode
@pytest.mark.flaky(reruns=20, reruns_delay=10)
def test_good_running_batman_server_mode(target, in_good_running, command):
    stdout, _, rc = command.run(
        "batctl gw_mode"
        )
    for l in stdout:
        if "server" in l:
            break
    else:
        assert False, "BATMAN Server mode not correct"


@pytest.mark.running_mode
@pytest.mark.flaky(reruns=20, reruns_delay=10)
def test_good_running_ping_v4_parker(target, in_good_running, config, command):
    stdout, _, rc = command.run(
        "ping -c 10 -4 gandolf.stratum0.org"
    )
    for l in stdout:
        if "transmitted" in l:
            match = re.search(r"(\d+) packets received", l, re.DOTALL)
            assert match, "Ping result unparseable"
            assert int(match[1]) > 5, "Packetloss is > 50%"
            break
    else:
        assert False, "Ping failed!"

@pytest.mark.running_mode
@pytest.mark.flaky(reruns=20, reruns_delay=10)
def test_good_running_ping_v6_parker(target, in_good_running, config, command):
    stdout, _, rc = command.run(
        "ping -c 10 -6 gandolf.stratum0.org"
    )
    for l in stdout:
        if "transmitted" in l:
            match = re.search(r"(\d+) packets received", l, re.DOTALL)
            assert match, "Ping result unparseable"
            assert int(match[1]) > 5, "Packetloss is > 50%"
            break
    else:
        assert False, "Ping failed!"

@pytest.mark.running_mode
def test_good_running_autoupdater(target, in_good_running, command):
    stdout, _, rc = command.run(
        "autoupdater -n",
        timeout=240,
    )
    assert rc == 0, "Autoupdater failed"

@pytest.mark.running_mode
@pytest.mark.lg_feature("dedicated_ports")
@pytest.mark.flaky(reruns=5, reruns_delay=10)
def test_good_running_dhcp(target, in_good_running, host_container):
    for _ in range(5):
        stdout, _, _ = host_container.run("ip -br a")
        if "10." in " ".join(stdout):
            break
        time.sleep(5)
    else:
        assert False, "No IP via DHCP received in time"

@pytest.mark.running_mode
@pytest.mark.lg_feature("dedicated_ports")
@pytest.mark.flaky(reruns=5, reruns_delay=10)
def test_good_running_ra(target, in_good_running, host_container):
    for _ in range(5):
        stdout, _, _ = host_container.run("ip -br -6 a")
        if "2001:bf7:381" in " ".join(stdout):
            break
        time.sleep(5)
    else:
        assert False, "No IPv6 via RA received in time"


@pytest.mark.running_mode
@pytest.mark.lg_feature("dedicated_ports")
def test_good_running_ping_gandolf6(target, in_good_running, host_container):
    for _ in range(1):
        stdout, _, rc = host_container.run(
            "ping -c 10 -6 gandolf.stratum0.org"
        )
        for l in stdout:
            if "transmitted" in l:
                match = re.search(r"(\d+) received", l, re.DOTALL)
                if match and int(match[1]) > 5:
                    return
        time.sleep(5)
    assert False, "Ping gandolf via v6 failed!"

@pytest.mark.running_mode
@pytest.mark.lg_feature("dedicated_ports")
def test_good_running_ping_gandolf4(target, in_good_running, host_container):
    for _ in range(5):
        stdout, _, rc = host_container.run(
            "ping -c 10 -4 gandolf.stratum0.org"
        )
        for l in stdout:
            if "transmitted" in l:
                match = re.search(r"(\d+) received", l, re.DOTALL)
                if match and int(match[1]) > 5:
                    return
        time.sleep(5)
    assert False, "Ping gandolf via v4 failed!"

@pytest.mark.running_mode
@pytest.mark.lg_feature("dedicated_ports")
def test_good_running_localnode_redirect(target, in_good_running, host_container, command):
    for _ in range(5):
        stdout, _, rc = host_container.run(
                "curl -v http://172.16.127.1/cgi-bin/status"
        )
        for l in stdout:
            match = re.search(r"^< Location: http:\/\/([10.\d.]*)\/", l, re.DOTALL)
            if match:
                ipv4_redir = match[1]
                break
        else:
            assert False, "No IPv4 redirect detected"

        stdout, _, rc = host_container.run(
                "curl -v http://[2001:bf7:382::1]/cgi-bin/status"
        )
        for l in stdout:
            match = re.search(r"^< Location: http:\/\/\[(2001:[0-9a-f:]*)", l, re.DOTALL)
            if match:
                ipv6_redir = match[1]
                break
        else:
            assert False, "No IPv6 redirect detected"

        stdout, _, rc = command.run("ip a show dev br-client")
        local_addrs = []
        for l in stdout:
            l = l.strip()
            if (l.startswith("inet ") and "global" in l) or (l.startswith("inet6 ") and "global" in l):
                local_addrs.append(l.split()[1].split("/")[0])

        if len(local_addrs) > 0 and ipv4_redir in local_addrs and ipv6_redir in local_addrs:
                break

    assert len(local_addrs) > 0, "No addreses on router found"
    assert ipv4_redir in local_addrs, "IPv4 redirect target not an address on br-client"
    assert ipv6_redir in local_addrs, "IPv6 redirect target not an address on br-client"

@pytest.mark.running_mode
@pytest.mark.lg_feature("dedicated_ports")
def test_good_running_icmp6_mtu(target, in_good_running, host_container, command):
    stdout, _, rc = host_container.run(
            "ip -br l | grep 'mv-' | cut -d '@' -f 1")
    interface = stdout[0]
    stdout, _, rc = host_container.run(
            f"tshark -nVi {interface} -f 'icmp6' -c 1 -T json -J 'icmpv6' 2>/dev/null",
            timeout=60,
    )

    for line in stdout:
        match = re.search(r"^[\s]*\"icmpv6.opt.mtu\":\s*\"([\d]+)\"", line, re.DOTALL)
        if match:
            mtu = match[1]
            break
    else:
        assert False, "No MTU option in icmpv6-package"

    assert int(mtu) == 1375, "MTU not as expected"

@pytest.mark.running_mode
def test_running_etc_bathosts_exists(in_good_running, command):
    _, _, rc = command.run("test -f /etc/bat-hosts")
    assert rc == 0, "/etc/bat-hosts does not exist"

@pytest.mark.running_mode
def test_running_etc_bathosts_update(in_good_running, command):
    _, _, rc = command.run("rm -f /etc/bat-hosts")
    assert rc == 0, "Failed to delete /etc/bat-hosts"

    stdout, _, rc = command.run("update-bat-hosts http://freifunk-bs.de/data/nodes.json")
    assert rc == 0, "Failed to run update-bat-hosts"
    assert not "".join(stdout).strip(), "Failed to run update-bat-hosts"

    _, _, rc = command.run("test -f /etc/bat-hosts")
    assert rc == 0, "/etc/bat-hosts does not exist"

    stdout, _, rc = command.run("du -b /etc/bat-hosts  | cut -f1")
    assert rc == 0
    assert int("".join(stdout)) > 100

# TODO: On Statuspage:
# * Test if correct wireguard key is shown
# * Test if contact info is shown if enabled
#@pytest.mark.running_mode
#@pytest.mark.lg_feature("dedicated_ports")
#def test_good_running_status_page(target, host_container):
#    ip = ipv6_link_local(command)
#    ifname = target.get_resource("RemoteNetworkInterface").ifname
#

