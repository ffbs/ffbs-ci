import enum
import logging
import os
import re
import time

import attr
import requests
import yaml
from labgrid.driver import SmallUBootDriver, ShellDriver, TFTPProviderDriver
from labgrid.factory import target_factory
from labgrid.protocol import PowerProtocol, ConsoleProtocol
from labgrid.step import step
from labgrid.strategy.common import Strategy
from pexpect import TIMEOUT


with open("parker_test_config.yaml") as fh:
    config = yaml.safe_load(fh)

@attr.s(cmp=False)
class StrategyError(Exception):
    msg = attr.ib(validator=attr.validators.instance_of(str))


class Status(enum.Enum):
    unknown = 0
    uboot = 1
    good_config = 2
    good_running = 3

"""
This Strategy is used to control a Freifunk-Gluon based Accesspoints through
its various states.

From every state it's possible to reach uboot. But most of the following
states depend on each other.

"""

@target_factory.reg_driver
@attr.s(eq=False)
class SmallUBootStrategy(Strategy):
    """UbootStrategy - Strategy to switch to uboot or shell"""
    bindings = {
        "power": PowerProtocol,
        "uboot": SmallUBootDriver,
        "shell": ShellDriver,
        "console": ConsoleProtocol,
        "tftp": TFTPProviderDriver,
    }

    status = attr.ib(default=Status.unknown)
    _flashed = False

    def __attrs_post_init__(self):
        super().__attrs_post_init__()
        self.logger = logging.getLogger("STRATEGY")

    @step(args=["status"])
    def transition(self, status, *, step):
        if not isinstance(status, Status):
            status = Status[status]
        if status == Status.unknown:
            raise StrategyError("can not transition to {}".format(status))
        elif status == self.status:
            return # nothing to do

        elif status == Status.uboot:
            # cycle power
            self.target.activate(self.power)
            self.power.cycle()
            # interrupt uboot
            self.target.activate(self.uboot)
            self.status = Status.uboot
        elif status == Status.good_config:
            if self._flashed:
                raise Exception("You try to re-flash via TFTP. You probably have a bug in your state-flow :(")

            # We'll flash this device using the "Wizard" in Uboot and not the interactive shell
            # To activate the wizard we need to power cycle the device and interrupt autoboot
            self.target.activate(self.power)
            self.power.off()
            self.target.deactivate(self.console)  # just to make sure the diver is deactivated
            self.target.activate(self.console)
            self.target.activate(self.tftp)
            self.logger.info("On our way to flash this device with the given gluon image.")
            self.logger.info("Power-cycling DUT and capture and interrupt boot.")
            self.power.cycle()

            def expect_helper(console, expect, timeout=8):
                index, _, _, _ = console.expect(
                    [expect, TIMEOUT],
                    timeout=timeout,
                )
                if index == 0:
                    return
                raise Exception('Timeout')

            expect_helper(self.console, 'Please choose the operation:')  # interrupt autoboot and select TFTP mode
            self.console.write('2'.encode())

            expect_helper(self.console, 'Are you sure?')
            self.console.write('Y'.encode())
            self.logger.info("Autoboot interrupt successful. Setting up TFTP...")

            expect_helper(self.console, r'Input device IP \(10.10.10.123\) ==:10\.10\.10\.123')
            self.console.write(('\b'*12).encode())  # backspace away the old ip
            self.console.sendline(self.target.env.config.get_option('tftp_dev_ip'))

            expect_helper(self.console, r'Input server IP \(10.10.10.3\) ==:10\.10\.10\.3')
            self.console.write(('\b'*12).encode())  # backspace away the old ip
            self.console.sendline(self.target.env.config.get_option('tftp_server_ip'))
            
            expect_helper(self.console, r'Input Linux Kernel filename \(\) ==:')
            p = self.tftp.stage(self.target.env.config.get_image_path('root'))
            self.console.sendline(p)

            self.logger.info("Waiting for TFTP to succeed and for Linux to boot...")
            expect_helper(self.console, 'Starting kernel ...', timeout=180)
            self.target.activate(self.shell)

            # that the shell allows us to log in, does not mean all services are ready -.-
            time.sleep(10)

            # wait for gluon-reconfigure to finish
            self.logger.info("Reached userspace. Waiting for gluon-reconfigure to finish reconfiguring gluon...")
            start = time.time()
            for _ in range(120):
                stdout = self.shell.run_check("ps")
                if not "/usr/bin/gluon-reconfigure" in " ".join(stdout):
                    break
                time.sleep(5)
            else:
                raise Exception("gluon-reconfigure took too long!")
            self.logger.info("Gluon-Reconfigure took {}s".format(time.time()-start))

            # wait until the config mode web-interface is ready
            self.logger.info("Waiting for the web-server to serve the config website...")
            for _ in range(20):
                try:
                    r = requests.get("http://192.168.1.1/", timeout=3)
                    break
                except Exception as e:
                    self.logger.debug(str(e))
                time.sleep(5)
            else:
                raise Exception("Config Mode: HTTP interface was not ready in time")
            self.logger.info("Config Mode HTTP ready. Config mode reached 🎉")

            self.status = Status.good_config
            self._flashed = True

        elif status == Status.good_running:
            self.transition(Status.good_config)

            self.logger.info("On our way to boot into production mode...")

            self.logger.info("Setting up our well-known wireguard keypair...")
            # setup static wireguard key
            self.shell.run_check("mkdir -p /etc/parker")
            self.shell.run_check("echo '{}' > /etc/parker/wg-privkey".format(config["wg_privkey"]))
            self.shell.run_check("echo '{}' > /etc/parker/wg-pubkey".format(config["wg_pubkey"]))

            self.logger.info("Setting up private Wifi using the web interface...")
            # setup private wifi
            payload = [
                ("token", ("", "")),
                ("id.1", ("", "1")),
                ("id.1.1.enabled", ("", "1")),
                ("id.1.1.ssid", ("", config["private_wifi_ssid"])),
                ("id.1.1.key", ("", config["private_wifi_psk"])),
                ("id.1.1.encryption", ("", "psk2")),
                ("id.1.1.mfp", ("", "0"))
            ]
            # Gluon requires a multipart/form-data instead of the usual application/x-www-form-urlencoded
            # To achieve this with requests, we must (mis)use the file parameter (instead of data)
            r = requests.post(
                "http://192.168.1.1/cgi-bin/config/admin/privatewifi",
                files=payload,
                headers={'Origin':'http://192.168.1.1'}
            )
            r.raise_for_status()

            self.logger.info("Setting up our SSH key using the web interface...")
            # append ci-ssh-key:
            keyfile = ssh_dir = os.path.join(
                os.environ['HOME'],
                ".ssh",
                "id_rsa.pub",
                )
            with open(keyfile) as fh:
                pubkey = fh.read()
            r = requests.get('http://192.168.1.1/cgi-bin/config/admin/remote')
            r.raise_for_status()
            match = re.search('<textarea[^>]+>(.+)</textarea>', r.text, re.DOTALL)
            if match:
                keys = match[1] + "\n" + pubkey
            else:
                keys = pubkey

            payload = [
                ("token", ("", "")),
                ("id.keys", ("", "1")),
                ("id.keys.1.keys", ("", keys))
            ]
            r = requests.post(
                "http://192.168.1.1/cgi-bin/config/admin/remote",
                files=payload,
                headers={'Origin':'http://192.168.1.1'}
            )
            r.raise_for_status()

            self.logger.info("Disabling the Autoupdater...")
            self.shell.run_check("uci set autoupdater.settings.enabled=0")
            self.shell.run_check("uci commit autoupdater")

            self.logger.info("Triggering the reboot using the webinterface...")
            # reboot via config mode web interface
            payload = [
                ("token", ("", "")),
                ("id.1", ("", "1")),
                ("id.1.4.hostname", ("", "ci_r6120")),
                ("id.1.5.meshvpn", ("", "1")),
                ("id.1.7.contact", ("", "Freifunk Braunschweig CI ffci-1"))
            ]
            r = requests.post(
                "http://192.168.1.1/cgi-bin/config/wizard",
                files=payload,
                headers={'Origin':'http://192.168.1.1'}
            )
            r.raise_for_status()

            if "<h2>Your node&#39;s setup is now complete.</h2>" in r.text:
                # config was complete. we will now boot our production system
                pass
            else:
                raise Exception(
                    "Failed to leave config mode to normal system. Last returned value was:\n{}".format(r.text)
                )

            # to detect the actual boot, we will stop at uboot
            self.logger.info("Waiting for uboot to take over control of this device")
            self.target.deactivate(self.shell)
            self.target.activate(self.uboot)

            # and ask uboot to start the system
            self.logger.info("Reached uboot. Booting Linux image and waiting for a useable shell...")
            self.uboot.boot("bc090000")
            self.uboot.await_boot()

            # after reaching the shell again we are set for more
            self.target.activate(self.shell)
            self.logger.info("We have reached userspace. Giving it some time to settle...")

            # give it some extra time to settle
            time.sleep(15)

            self.logger.info("Checking, if we are still in config mode...")
            stdout = self.shell.run_check("uci get 'gluon-setup-mode.@setup_mode[0].configured'")[0].strip()
            if stdout == "0":
                raise Exception("Node still in config mode. Failed to reach production mode!")
            self.logger.info("Nope, not in config mode.")

            start = time.time()
            # checking if noderoute and nodeconfig have finished
            # later we could also use this to raise an error if they don't - but for now
            # the feature in the firmware is a little too new.
            for _ in range(20):
                _, _, rc = self.shell.run("[ -e /tmp/ff-Ohb0ba0u/nodeconfig-successful ]")
                if rc == 0:
                    self.logger.info("Nodeconfig finished after {}s".format(time.time()-start))
                    break
                time.sleep(5)
            else:
                self.logger.error("nodeconfig did not finish in time. Continuing anyway.")
            for _ in range(20):
                _, _, rc = self.shell.run("[ -e /tmp/ff-Ohb0ba0u/noderoute-successful ]")
                if rc == 0:
                    self.logger.info("noderoute finished after {}s".format(time.time()-start))
                    break
                time.sleep(5)
            else:
                self.logger.error("noderoute did not finish in time. Continuing anyway.")

            self.status = Status.good_running
            self.logger.info("Production mode reached 🎉")
            self.logger.info("Remember: We did not check for all the services to come up. "
                             "Handling this is up to the tests.")

        else:
            raise StrategyError("no transition found from {} to {}".format(
                self.status,
                status
            ))

    @step(args=["status"])
    def force(self, status, *, step):
        if not isinstance(status, Status):
            status = Status[status]
        if status == Status.unknown:
            raise StrategyError("can not force state {}".format(status))
        elif status == Status.good_running:
            self.target.activate(self.power)
            self.target.activate(self.shell)
        else:
            raise StrategyError("not setup found for {}".format(status))
        self.status = status
