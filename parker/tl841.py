import os
import enum
import attr
import re
import requests
import time
import logging
logger = logging.getLogger(__name__)
import yaml

from labgrid.driver import SmallUBootDriver, ShellDriver, TFTPProviderDriver
from labgrid.protocol import PowerProtocol
from labgrid.factory import target_factory
from labgrid.strategy.common import Strategy
from labgrid.step import step

with open("parker_test_config.yaml") as fh:
    config = yaml.safe_load(fh)

@attr.s(cmp=False)
class StrategyError(Exception):
    msg = attr.ib(validator=attr.validators.instance_of(str))


class Status(enum.Enum):
    unknown = 0
    uboot = 1
    good_config = 2
    good_running = 3

"""
This Strategy is used to control a Freifunk-Gluon based accespoints through
its various states.

From every state it's possible to reach uboot. But most of the following
states depend on each other.

"""

@target_factory.reg_driver
@attr.s(eq=False)
class SmallUBootStrategy(Strategy):
    """UbootStrategy - Strategy to switch to uboot or shell"""
    bindings = {
        "power": PowerProtocol,
        "uboot": SmallUBootDriver,
        "shell": ShellDriver,
        "tftp": TFTPProviderDriver,
    }

    status = attr.ib(default=Status.unknown)
    _flashed = False

    def __attrs_post_init__(self):
        super().__attrs_post_init__()

    @step(args=["status"])
    def transition(self, status, *, step):
        if not isinstance(status, Status):
            status = Status[status]
        if status == Status.unknown:
            raise StrategyError("can not transition to {}".format(status))
        elif status == self.status:
            return # nothing to do

        elif status == Status.uboot:
            # cycle power
            self.target.activate(self.power)
            self.power.cycle()
            # interrupt uboot
            self.target.activate(self.uboot)
            self.status = Status.uboot
        elif status == Status.good_config:
            # transition to uboot

            if self._flashed:
                raise  Exception("You try to re-flash via TFTP. You probably have a bug in your state-flow :(")

            self.transition(Status.uboot)
            self.target.activate(self.tftp)

            # flash the known good image
            # we assume some external logic has put good.bin in place
            time.sleep(5)
            img = self.tftp.stage(self.target.env.config.get_image_path('root'))
            for _ in range(3):
                result, _, _ = self.uboot.run(f"tftpboot 0x80000000 {img}")
                if 'Bytes transferred'  in result[-1]:
                    break
            else:
                raise Exception("Could not load image via TFTP. Last output was: {}".format(result))
            self.uboot.run("erase 0x9f020000 +0x3c0000")
            self.uboot.run("cp.b 0x80000000 0x9f020000 0x3c0000")
            self.uboot.boot("0x9f020000")
            self.uboot.await_boot()
            self.target.activate(self.shell)

            # that the shell responses does not mean all services are ready -.-
            time.sleep(10)

            # wait for gluon-reconfigure to finish
            start = time.time()
            for _ in range(120):
                stdout, _, _ = self.shell.run("ps")
                if not "/usr/bin/gluon-reconfigure" in " ".join(stdout):
                    break
                time.sleep(5)
            else:
                raise Exception("glon-reconfigure took too long!")
            print("Gluon-Reconfigure took {}s".format(time.time()-start))

            # wait until the config mode web-interface is ready
            for _ in range(20):
                try:
                    print("")
                    r = requests.get("http://192.168.1.1/", timeout=3)
                    break
                except Exception as e:
                    print(e)
                time.sleep(5)
            else:
                raise Exception("Config Mode: HTTP interface was not ready in time")
            print("Config Mode HTTP ready")

            self.status = Status.good_config
            self._flashed = True

        elif status == Status.good_running:
            self.transition(Status.good_config)

            # setup static wireguard key
            stdout, _, rc = self.shell.run("echo '{}' > /etc/parker/wg-privkey".format(config["wg_privkey"]))
            assert rc == 0
            stdout, _, rc = self.shell.run("echo '{}' > /etc/parker/wg-pubkey".format(config["wg_pubkey"]))
            assert rc == 0

            # setup private wifi
            payload = [
                ("token", ("", "")),
                ("id.1", ("", "1")),
                ("id.1.1.enabled", ("", "1")),
                ("id.1.1.ssid", ("", config["private_wifi_ssid"])),
                ("id.1.1.key", ("", config["private_wifi_psk"])),
                ("id.1.1.encryption", ("", "psk2")),
                ("id.1.1.mfp", ("", "0"))
            ]
            # Gluon requires a multipart/form-data instead of the usual application/x-www-form-urlencoded
            # To achieve this with requests, we must (mis)use the files parameter (instead of data)
            r = requests.post("http://192.168.1.1/cgi-bin/config/admin/privatewifi", files=payload, headers={'Origin':'http://192.168.1.1'})

            # append ci-ssh-key:
            keyfile = ssh_dir = os.path.join(
                os.environ['HOME'],
                ".ssh",
                "id_rsa.pub",
                )
            with open(keyfile) as fh:
                pubkey = fh.read()
            r = requests.get('http://192.168.1.1/cgi-bin/config/admin/remote')
            match = re.search('<textarea[^>]+>(.+)</textarea>', r.text, re.DOTALL)
            if match:
                keys = match[1] + "\n" + pubkey
            else:
                keys = pubkey

            payload = [
                ("token", ("", "")),
                ("id.keys", ("", "1")),
                ("id.keys.1.keys", ("", keys))
            ]
            r = requests.post("http://192.168.1.1/cgi-bin/config/admin/remote", files=payload, headers={'Origin':'http://192.168.1.1'})

            # reboot via config mode web interface
            payload = [
                ("token", ("", "")),
                ("id.1", ("", "1")),
                ("id.1.4.hostname", ("", "ci_841_v11")),
                ("id.1.5.meshvpn", ("", "1")),
                ("id.1.7.contact", ("", "Freifunk Braunschweig CI ffci-1"))
            ]
            r = requests.post("http://192.168.1.1/cgi-bin/config/wizard", files=payload, headers={'Origin':'http://192.168.1.1'})

            if "<h2>Your node&#39;s setup is now complete.</h2>" in r.text:
                # config was complete. we will now boot our production system
                pass
            else:
                raise Exception("Failed to leave config mode to normal system. Last returned value was:\n{}".format(r.text))

            # to detect the actual boot we will stop at uboot
            self.target.activate(self.uboot)

            # and ask uboot to start the system
            self.uboot.boot("0x9f020000")
            self.uboot.await_boot()

            # after reaching the shell again we are set for more
            self.target.activate(self.shell)

            # give it some extra time to settle
            time.sleep(15)

            self.status = Status.good_running

        else:
            raise StrategyError("no transition found from {} to {}".format(
                self.status,
                status
            ))

    @step(args=["status"])
    def force(self, status, *, step):
        if not isinstance(status, Status):
            status = Status[status]
        if status == Status.unknown:
            raise StrategyError("can not force state {}".format(status))
        elif status == Status.good_running:
            self.target.activate(self.power)
            self.target.activate(self.shell)
        else:
            raise StrategyError("not setup found for {}".format(status))
        self.status = status
