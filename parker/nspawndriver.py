import logging
import warnings
import subprocess
import time

import attr
from pexpect import TIMEOUT
import serial
import serial.rfc2217

from labgrid.factory import target_factory
from labgrid.protocol import ConsoleProtocol
from labgrid.driver.common import Driver
from labgrid.driver.consoleexpectmixin import ConsoleExpectMixin
from labgrid.util.proxy import proxymanager
from labgrid.resource import SerialPort


@target_factory.reg_driver
@attr.s(eq=False)
class NSpawnDriver(ConsoleExpectMixin, Driver, ConsoleProtocol):
    """
    Driver implementing the ConsoleProtocol interface for a systemd-nspawn container
    """
    container_name = attr.ib(validator=attr.validators.instance_of(str))
    command = attr.ib(default="systemd-nspawn", validator=attr.validators.instance_of(str))
    command_prefix = attr.ib(default="", validator=attr.validators.instance_of(str))
    network_interface = attr.ib(default="", validator=attr.validators.instance_of(str))
    ephemeral_mode = attr.ib(default=True, validator=attr.validators.instance_of(bool))

    txdelay = 0.0

    def __attrs_post_init__(self):
        super().__attrs_post_init__()
        self.logger = logging.getLogger("{}({})".format(self, self.container_name))
        self.process = None

    def on_activate(self):
        # spawn container in subprocess
        if self.process:
            raise Exception("Nspawn-Driver is already activated.")
        args = []
        if len(self.command_prefix) > 0:
            args.append(self.command_prefix)
        args.append(self.command)
        args.append("-b")
        args.append("--console=interactive")
        if self.ephemeral_mode:
            args.append("-x")
        args.append("--machine={}".format(self.container_name))
        if len(self.network_interface) > 0:
            args.append("--network-macvlan={}".format(self.network_interface))
        self.logger.info("Starting container " +str(args))
        self.process = subprocess.Popen(
                args, 
                bufsize=0, 
                stdin=subprocess.PIPE,
                stderr=subprocess.STDOUT, 
                stdout=subprocess.PIPE,
                )
        self._poll()

    def on_deactivate(self):
        self._poll()
        if self.process:
            subprocess.run(["sudo", "kill", str(self.process.pid)])
            for _ in range(5):
                if not self.process.poll():
                    time.sleep(1)
                else:
                    print("systemd-nspawn container exited in time. thanks! =)")
                    break
            else:
                print("systemd-nspawn failed to exit in time. killing {}".format(self.process.pid))
                subprocess.run(["sudo", "kill", "-9", str(self.process.pid)])
            self.process = None

    def _read(self, size: int = 1, timeout: float = 0.0, max_size=None):
        """
        Reads 'size' or more bytes from the subprocess's stdout

        Keyword Arguments:
        size -- amount of bytes to read, defaults to 1
        """
        if max_size:
            read_size = min(size, max_size)
        else:
            read_size = size

        self._poll()
        return self.process.stdout.read(read_size)

    def _write(self, data: bytes):
        """
        Writes 'data' to the serialport

        Arguments:
        data -- data to write, must be bytes
        """
        self._poll()
        self.process.stdin.write(data)
        self.process.stdin.flush()

    def _poll(self):
        if self.process.poll():
            self.logger.error("systemd-nspawn container {} terminated unexpected!".format(self.container_name))
            raise Exception("systemd-nspan container {} terminated unexpected!".format(self.container_name))

    def __str__(self):
        return f"NSpawnDriver({self.target})"
