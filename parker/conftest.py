import pytest
import yaml
import re
import os

def pytest_configure(config):
    config.addinivalue_line("markers", "config_mode")
    config.addinivalue_line("markers", "running_mode")

@pytest.fixture
def fw_good(request):
    return request.config.getoption("--good")

@pytest.fixture(scope="session")
def config():
    with open("parker_test_config.yaml") as fh:
        config = yaml.safe_load(fh)
        return config

@pytest.fixture(scope="function")
def in_uboot(strategy):
    strategy.transition("uboot")

@pytest.fixture(scope="function")
def in_good_config(strategy):
    strategy.transition("good_config")

@pytest.fixture(scope="function")
def in_good_running(strategy):
    strategy.transition("good_running")

@pytest.fixture(scope="function")
def command(target):
    return target.get_driver("ShellDriver", name="dut_shell")

@pytest.fixture(scope="function")
def host_container(target):
    container = target.get_driver("NSpawnDriver")
    shell = target.get_driver("ShellDriver", name="container_shell")
    target.activate(shell)

    yield shell

    target.deactivate(shell)
    target.deactivate(container)

@pytest.fixture(scope="module")
def info_pull_ssh_key(target):
    """
    This fixture sets up our known ssh-key for the info-pull user.
    """

    command = target.get_driver("ShellDriver", name="dut_shell")
    auth_keys_file = "/usr/share/info-pull/.ssh/authorized_keys"
    # read previous file
    oldkey, _, rc = command.run(f"cat {auth_keys_file}")
    assert rc == 0, "Could not read previous info-pull authorized_keys file"

    # read our ssh pubkey
    keyfile = ssh_dir = os.path.join(
        os.environ['HOME'],
        ".ssh",
        "id_rsa.pub",
        )
    with open(keyfile) as fh:
        pubkey = fh.read()

    # append our key with the original command
    assert len(oldkey) >= 1, "Could not read previous command part"
    assert " ssh-" in oldkey[0], "Could not read previous command part"
    command_part = oldkey[0].split(" ssh-")[0]
    newkey = f"{command_part} {pubkey}"

    while newkey:
        _, _, rc = command.run(f"echo -n '{newkey[:300]}' >> {auth_keys_file}")
        newkey = newkey[300:]
    assert rc == 0


def ipv6_link_local(command):
    stdout, _, code = command.run('ip address show br-client')
    if code != 0:
        raise Exception('Failed to get address from br-client:\n{}'.format(''.join(stdout)))
    match = re.search('fe80::[^/]+', ''.join(stdout))
    if not match:
        raise Exception('Failed to get address from br-client: Interface does not have a link-local address')
    return match[0]

