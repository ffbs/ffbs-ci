#!/usr/bin/env python3
import sys
from string import digits, ascii_letters as letters

def char_order(c):
    if c in digits:
        return 0
    elif c in letters:
        return ord(c)
    elif c == '~':
        return -1
    else:
        return ord(c) + 256

def newer_than(A, B):
    if not A: return False
    if not B: return True

    a = 0
    b = 0

    while a < len(A) and b < len(B):
        first_diff = 0

        # compare non-digits character by character
        while (a < len(A) and A[a] not in digits) or (b < len(B) and B[b] not in digits):
            ac = char_order(A[a])
            bc = char_order(B[b])
            if ac != bc:
                return ac > bc
            a += 1
            b += 1

        # ignore leading zeroes
        while A[a] == '0':
            a += 1
        while B[b] == '0':
            b += 1

        # compare numbers digit by digit, but don't return yet in case
        # one number is longer (and thus larger) than the other
        while A[a] in digits and B[b] in digits:
            if first_diff == 0:
                first_diff = ord(A[a]) - ord(B[b])
            a += 1
            b += 1

        # check if one number is larger
        if A[a] in digits:
            return True
        if B[b] in digits:
            return False

        if first_diff != 0:
            return first_diff > 0

    return False

if __name__ == '__main__':
    # USAGE: newer older
    if newer_than(sys.argv[1], sys.argv[2]):
        print('newer')
    else:
        print('older')
