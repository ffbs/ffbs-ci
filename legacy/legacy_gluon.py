import enum

import attr

import requests

import time

from labgrid.driver import SmallUBootDriver, ShellDriver, GpioDigitalOutputDriver
from labgrid.protocol import PowerProtocol
from labgrid.factory import target_factory
from labgrid.strategy.common import Strategy


@attr.s(cmp=False)
class StrategyError(Exception):
    msg = attr.ib(validator=attr.validators.instance_of(str))


class Status(enum.Enum):
    unknown = 0
    uboot = 1
    good_config = 2
    good_running = 3
    good_running_setfastd = 4
    good_config_preNew = 5
    new_running = 6
    new_config = 7

"""
This Strategy is used to control a Freifunk-Gluon based accespoints through
its various states.

From every state it's possible to reach uboot. But most of the following
states depend on each other.

"""

@target_factory.reg_driver
@attr.s(cmp=False)
class SmallUBootStrategy(Strategy):
    """UbootStrategy - Strategy to switch to uboot or shell"""
    bindings = {
        "power": PowerProtocol,
        "uboot": SmallUBootDriver,
        "shell": ShellDriver,
    }

    status = attr.ib(default=Status.unknown)
    _flashed = False

    def __attrs_post_init__(self):
        super().__attrs_post_init__()

    def transition(self, status):
        if not isinstance(status, Status):
            status = Status[status]
        if status == Status.unknown:
            raise StrategyError("can not transition to {}".format(status))
        elif status == self.status:
            return # nothing to do

        elif status == Status.uboot:
            # make sure reset is not asserted
            reset = self.target.get_driver(GpioDigitalOutputDriver)
            reset.set(True)

            # cycle power
            self.target.activate(self.power)
            self.power.cycle()
            # interrupt uboot
            self.target.activate(self.uboot)
            self.status = Status.uboot
        elif status == Status.good_config:
            # transition to uboot

            if self._flashed:
                raise  Exception("You try to re-flash via TFTP. You probably have a bug in your state-flow :(")

            self.transition(Status.uboot)

            # flash the known good image
            # we assume some external logic has put good.bin in place
            time.sleep(5)
            for _ in range(3):
                result, _, _ = self.uboot.run("tftpboot 0x80000000 good.bin")
                if 'Bytes transferred'  in result[-1]:
                    break
            else:
                raise Exception("Could not load image via TFTP. Last output was: {}".format(result))
            self.uboot.run("erase 0x9f020000 +0x3c0000")
            self.uboot.run("cp.b 0x80000000 0x9f020000 0x3c0000")
            self.uboot.boot("0x9f020000")
            self.uboot.await_boot()
            self.target.activate(self.shell)
            # that the shell responses does not mean all services are ready -.-
            time.sleep(10)
            self.status = Status.good_config

            self._flashed = True

        elif status == Status.good_running:
            self.transition(Status.good_config)

            payload = {
                "token": "",
                "id.1":"1",
                "id.1.4.hostname": "ci_841_v11",
                "id.1.5.meshvpn":"1",
                "id.1.6.contact": ""
            }
            r = requests.post("http://192.168.1.1/cgi-bin/config/wizard", params=payload)

            if "<h2>Your node&#39;s setup is now complete.</h2>" in r.text:
                # config was complete. we will now boot our production system
                pass
            else:
                raise Exception("Failed to leave config mode to normal system. Last returned value was:\n{}".format(r.text))

            # to detect the actual boot we will stop at uboot
            self.target.activate(self.uboot)

            # and ask uboot to start the system
            self.uboot.boot("0x9f020000")
            self.uboot.await_boot()

            # after reaching the shell again we are set for more
            self.target.activate(self.shell)

            # give it some extra time to settle
            time.sleep(15)

            self.status = Status.good_running



        elif status == Status.good_running_setfastd:
            self.transition(Status.good_running)

            self.shell.run("reboot now")

            # to detect the actual boot we will stop at uboot
            self.target.activate(self.uboot)

            # and ask uboot to start the system
            self.uboot.boot("0x9f020000")
            self.uboot.await_boot()

            # after reaching the shell again we are set for more
            self.target.activate(self.shell)

            # give it some extra time to settle
            time.sleep(15)

            self.status = status

        elif status == Status.good_config_preNew:
            self.transition(Status.good_running_setfastd)

            # lets "press" the reset-button
            reset = self.target.get_driver(GpioDigitalOutputDriver)
            reset.set(False)

            # we need to press the reset-button for some seconds
            # but we need to make sure that it is released when entering uboot
            time.sleep(6)

            #release reset
            reset.set(True)

            # to detect the actual boot we will stop at uboot
            self.target.activate(self.uboot)

            # and ask uboot to start the system
            self.uboot.boot("0x9f020000")
            self.uboot.await_boot()

            # after reaching the shell again we are set for more
            self.target.activate(self.shell)

            # give it some extra time to settle
            time.sleep(15)

            self.status = status
        elif status == Status.new_running:
            self.transition(Status.good_config_preNew)

            ul_images = {"image" : open("/srv/tftp/new.bin", "rb")}
            ul_payload = (('step', '2'), ('token', ''), ('keepcfg', '1'))
            r = requests.post(
                "http://192.168.1.1/cgi-bin/config/admin/upgrade",
                files=ul_images,
                data=ul_payload
            )
            if "The firmware image has been transmitted." in r.text:
                # upload was successfully.
                # we must provide requests with a file. otherwise it will
                # not do a Mime-Multipart upload.
                # reading /dev/null seems to work fine for this
                ack_payload = (('step', '3'), ('token', ''), ('keepcfg', '1'))
                ack_images = {"abc": open("/dev/null", "rb")}
                r = requests.post(
                    "http://192.168.1.1/cgi-bin/config/admin/upgrade",
                    files=ack_images,
                    data=ack_payload
                )
                # upgrading takes soooo long...
                time.sleep(25)
                self.target.activate(self.uboot)
                self.uboot.boot("0x9f020000")
                self.uboot.await_boot()
                self.target.activate(self.shell)
                self.status = Status.new_running
            else:
                raise Exception("Falied to upload new firmware via HTTP")
        elif status == Status.new_config:
            self.transition(Status.new_running)

            # lets "press" the reset-button
            reset = self.target.get_driver(GpioDigitalOutputDriver)
            reset.set(False)

            # we need to press the reset-button for some seconds
            # but we need to make sure that it is released when entering uboot
            time.sleep(4)

            #release reset
            reset.set(True)

            # to detect the actual boot we will stop at uboot
            self.target.activate(self.uboot)

            # and ask uboot to start the system
            self.uboot.boot("0x9f020000")
            self.uboot.await_boot()

            # after reaching the shell again we are set for more
            self.target.activate(self.shell)

            # give it some extra time to settle
            time.sleep(15)

            self.status = status
        else:
            raise StrategyError("no transition found from {} to {}".format(
                self.status,
                status
            ))




