import pytest
import requests
import time
import os
import shutil
import gluon_version
import subprocess
import yaml

import logging
l = logging.Logger("pytest")

from labgrid import StepReporter
StepReporter()

@pytest.fixture()
def config():
    with open("test_config.yaml") as fh:
        config = yaml.load(fh)
        return config

@pytest.fixture()
def strategy(target):
    try:
        return target.get_driver('SmallUBootStrategy')
    except:
        pytest.skip("strategy not found")

@pytest.fixture(scope="function")
def in_uboot(strategy):
    strategy.transition("uboot")

@pytest.fixture(scope="function")
def in_good_config(strategy):
    strategy.transition("good_config")

@pytest.fixture(scope="function")
def in_good_running(strategy):
    strategy.transition("good_running")

@pytest.fixture(scope="function")
def in_good_running_setfastd(strategy):
    strategy.transition("good_running_setfastd")

@pytest.fixture(scope="function")
def in_good_config_preNew(strategy):
    strategy.transition("good_config_preNew")

@pytest.fixture(scope="function")
def in_new_running(strategy):
    strategy.transition("new_running")

@pytest.fixture(scope="function")
def in_new_config(strategy):
    strategy.transition("new_config")

uci_should_persist = [
    ("wireless.wan_radio0", "wifi-iface"),
    ("wireless.wan_radio0.disabled", "0"),
    ("wireless.wan_radio0.macaddr", "72:4c:49:0a:78:eb"),
    ("wireless.wan_radio0.network", "wan"),
    ("wireless.wan_radio0.encryption", "psk2"),
    ("wireless.wan_radio0.device", "radio0"),
    ("wireless.wan_radio0.mode", "ap"),
    ("wireless.wan_radio0.key", "stehtinderinfoecke"),
    ("wireless.wan_radio0.ssid", "meinwlan"),
]

def assert_web(url, text_to_find):
    r = requests.get(url)
    assert r.status_code == 200, "Trying to get url {} failed with status code {}. Was looking for text '{}' there...".format(url, r.status_code, text_to_find)
    assert text_to_find in r.text, "Could not find '{}' in {}. Site returned:\n{}".format(text_to_find, url, r.text)


def assert_not_web(url, text_to_find):
    r = requests.get(url)
    assert r.status_code == 200, "Trying to get url {} failed with status code {}. Was looking for text '{}' there...".format(url, r.status_code, text_to_find)
    assert not text_to_find in r.text, "Could find '{}' in {}. Site returned:\n{}".format(text_to_find, url, r.text)






def test_good_exists(target, fw_good, config):
    if not os.path.isfile(os.path.join(config["fw_src"], fw_good)):
        pytest.exit("Could not find good firmware")

def test_new_exists(target, fw_new, config):
    if not os.path.isfile(os.path.join(config["fw_src"], fw_new)):
        pytest.exit("New firmware not found")

def test_copy_good_to_tftp(target, fw_good, config):
    src = os.path.join(config["fw_src"], fw_good)
    dest = os.path.join(config["fw_tftp"], "good.bin")
    try:
        if os.path.exists(dest):
            os.remove(dest)
        shutil.copy2(src, dest)
    except Exception as e:
        pytest.exit("Failed to copy known-good firmware to tftp-server:"+str(e))

def test_copy_new_to_tftp(target, fw_new, config):
    src = os.path.join(config["fw_src"], fw_new)
    dest = os.path.join(config["fw_tftp"], "new.bin")
    try:
        if os.path.exists(dest):
            os.remove(dest)
        shutil.copy2(src, dest)
    except Exception as e:
        pytest.exit("Failed to copy new firmware to tftp-server:"+str(e))

#TODO: Fix broken version test
#def test_new_is_newer(target, fw_new, fw_good):
#	assert gluon_version.newer_than(fw_new, fw_good), "New firmware does not seem newer than known good. Is this intended?"

def get_fw_branch(fw_name):
    names = ["stable", "beta", "experimental"]

    for name in names:
        if name in fw_name:
            return name

def test_fw_same_branch(target, fw_new, fw_good):
    assert get_fw_branch(fw_new) == get_fw_branch(fw_good), "New and good firmware mismatch in branch. Is this intended?"





def test_uboot(target, in_uboot):
    #command = target.get_driver(CommandProtocol)
    command = target.get_driver('UBootDriver')

    stdout, stderr, returncode = command.run('version')
    assert returncode == 0
    assert len(stdout) > 0
    assert len(stderr) == 0
    assert 'U-Boot' in '\n'.join(stdout)

def test_help(target, in_uboot):
    #command = target.get_driver(CommandProtocol)
    command = target.get_driver('UBootDriver')

    stdout, stderr, returncode = command.run('help')
    assert returncode == 0
    assert len(stdout) > 0
    assert len(stderr) == 0
    assert 'boot' in '\n'.join(stdout)

def test_good_config_await_uhttpd_ready(target, in_good_config):

    # give it some extra time...
    time.sleep(15)

def test_good_config_default_coordinates(target, in_good_config, config):
    assert_web("http://192.168.1.1/cgi-bin/config/wizard", config["location_lat"])
    assert_web("http://192.168.1.1/cgi-bin/config/wizard", config["location_lon"])

def test_good_config_altitude_field(target, in_good_config):
    assert_web("http://192.168.1.1/cgi-bin/config/wizard", "id.1.6.altitude")

def test_good_config_no_contact(target, in_good_config):
    assert_not_web("http://192.168.1.1/cgi-bin/config/wizard", "id.1.6.contact")

def test_good_config_autoupdater_enabled(target, in_good_config):
    assert_web("http://192.168.1.1/cgi-bin/config/admin/autoupdater", 'value="1" id="id.1.1.enabled" name="id.1.1.enabled" checked="checked"')

def test_good_config_set_ssh_pubkey(target, in_good_config, config):
    with open(config["ssh_pubkey_file"]) as fh:
        key = fh.read()

    payload = {
        "token": "",
        "id.keys": "1",
        "id.keys.1.keys": key
    }

    r = requests.post("http://192.168.1.1/cgi-bin/config/admin/remote", params=payload)

    assert r.ok, "Could not place ssh key in web interface"
    assert r.status_code == 200 , "Return code missmatch"

def test_good_running_fastd_set_key(target, in_good_running, config):
    command = target.get_driver("ShellDriver")

    stdout, _, _ = command.run("uci set {}='{}'".format("fastd.mesh_vpn.secret", config["fastd_secret"]))
    assert len(stdout) == 0, "uci set returned unparseable result while setting {} to {}: {}".format(uci_key, uci_value, stdout)

    stdout, _, _ = command.run("uci commit")
    assert len(stdout) == 0, "uci set returned unparseable result while committing change: {}".format(uci_key)

@pytest.mark.parametrize("uci_key, uci_value", uci_should_persist)
def test_good_running_set_uci(target, in_good_running, uci_key, uci_value):
    command = target.get_driver("ShellDriver")

    stdout, _, _ = command.run("uci set {}='{}'".format(uci_key, uci_value))
    assert len(stdout) == 0, "uci set returned unparseable result while setting {} to {}: {}".format(uci_key, uci_value, stdout)

    stdout, _, _ = command.run("uci commit")
    assert len(stdout) == 0, "uci set returned unparseable result while committing change: {}".format(uci_key)

def test_good_running_fastd_active(target, in_good_running):
    command = target.get_driver("ShellDriver")
    stdout, _, _ = command.run("ls /etc/rc.d/ | grep fastd")
    assert stdout[0] == "S95fastd", "could not enable fastd"

def test_good_running_fastd_key_generated(target, in_good_running):
    command = target.get_driver("ShellDriver")
    stdout, _, _ = command.run("uci show fastd.mesh_vpn.secret")
    line = "".join(stdout)
    if len(line.split("'")) == 3:
        assert not int(line.split("'")[1], 16) == 0, "no fastd key was generated."

@pytest.mark.flaky(reruns=10, reruns_delay=10)
def test_good_running_ssh(target, in_good_running, config):
    r = subprocess.run(["ssh", "-o", "UserKnownHostsFile=/dev/null", "-o", "StrictHostKeyChecking=no", "-o", "PreferredAuthentications=publickey", "-i", config["ssh_key_file"], config["ssh_connect_host_running"], "echo 1"])

    assert r.returncode == 0, "SSH connection during run mode failed"


def test_good_runnin_setfastd_fastd_active(target, in_good_running_setfastd):
    command = target.get_driver("ShellDriver")
    stdout, _, _ = command.run("ls /etc/rc.d/ | grep fastd")
    assert stdout[0] == "S95fastd", "could not enable fastd"

def test_good_running_setfastd_fastd_key_generated(target, in_good_running_setfastd):
    command = target.get_driver("ShellDriver")
    stdout, _, _ = command.run("uci show fastd.mesh_vpn.secret")
    line = "".join(stdout)
    if len(line.split("'")) == 3:
        assert not int(line.split("'")[1], 16) == 0, "no fastd key was generated."



def test_good_preNew_await_uhttpd_ready(target, in_good_config_preNew):
    # give it some extra time...
    time.sleep(15)

def test_good_preNew_correct_site(target, in_good_config_preNew, config):
    assert_web("http://192.168.1.1/cgi-bin/config/admin/info", config["site_name"])

#TODO fix version handling.
#def test_new_running_correct_version(target, in_new_running, fw_good):
#    fwversion = fw_good.split("-")[0]
#
#    command = target.get_driver("ShellDriver")
#    targetversion, _, _ = command.run("cat /lib/gluon/gluon-version")
#
#    assert fwversion == targetversion[0], "FW-Version on the target did not match filename of known-good after flashing."

def test_new_running_correct_branch(target, in_new_running, fw_good):
    fwbranch = get_fw_branch(fw_good)

    command = target.get_driver("ShellDriver")
    targetbranch, _, _ = command.run("cat /lib/gluon/autoupdater/default_branch")

    assert targetbranch[0] == fwbranch, "FW-branch on the target did not match filename of known-good after flashing."

@pytest.mark.parametrize("uci_key,uci_value,should_be_set", [
    ("wireless.client_radio0.ssid", "Freifunk", True),
    ("wireless.client_radio0.disabled", "0", True),
    ("wireless.client_radio0.mode", "ap", True),

    ("wireless.mesh_radio0.disabled", "0", True),
    ("wireless.mesh_radio0.mesh_id", "ffbs-mesh", True),
    ("wireless.mesh_radio0.mode", "mesh", True),

    ("wireless.radio0.supported_rates", "'1000'", False),
    ("wireless.radio0.supported_rates", "'2000'", False),
    ("wireless.radio0.basic_rate", "'1000'", False),
    ("wireless.radio0.basic_rate", "'2000'", False)
] + [x + (True,) for x in uci_should_persist])
def test_good_wifi_uci_config(target, in_new_running, uci_key, uci_value, should_be_set):
    command = target.get_driver("ShellDriver")

    stdout, _, _ = command.run("uci show {}".format(uci_key))

    key_value_pairs = []
    test_key = None
    test_value = None

    for line in stdout:
        kv = line.split('=')
        if kv[0] == uci_key:
            test_key = kv[0]
            test_value = kv[1].replace("'","").replace('"','')


    assert not stdout == "uci: Entry not found", "UCI does not contain entry: {}".format(uci_key)
    if should_be_set:
        assert test_key == uci_key, "UCI Key {} probably not found in output: {}".format(uci_key, stdout)
        assert test_value == uci_value, "UCI Key {} does not contain value {} but returned {} instead".format(uci_key, uci_value, stdout)
    else:
        assert test_value != uci_value, "UCI field {} is set to value {}".format(uci_key, uci_value)

def test_good_wifi_uci_config_fastd(target, in_new_running, config):
    command = target.get_driver("ShellDriver")

    uci_key = "fastd.mesh_vpn.secret"
    uci_value = config["fastd_secret"]

    stdout, _, _ = command.run("uci show {}".format(uci_key))

    key_value_pairs = []
    test_key = None
    test_value = None

    for line in stdout:
        kv = line.split('=')
        if kv[0] == uci_key:
            test_key = kv[0]
            test_value = kv[1].replace("'","").replace('"','')


    assert not stdout == "uci: Entry not found", "UCI does not contain entry: {}".format(uci_key)
    assert test_key == uci_key, "UCI Key {} probably not found in output: {}".format(uci_key, stdout)
    assert test_value == uci_value, "UCI Key {} does not contain value {} but returned {} instead".format(uci_key, uci_value, stdout)

def test_new_running_disable_mesh0(target, in_new_running):
    command = target.get_driver("ShellDriver")

    # give BATMAN time to settle
    time.sleep(20)

    stdout, _, _ = command.run("ip l set mesh0 down")
    assert len("".join(stdout)) == 0, "Could not disable mesh0 for testing of wired uplink"

    # give BATMAN time to settle
    time.sleep(20)

def test_new_running_check_mesh0_down(target, in_new_running):
    command = target.get_driver("ShellDriver")

    stdout, _, _ = command.run("ip l | grep mesh0")

    assert "DOWN" in " ".join(stdout), "Failed to get mesh0 DOWN"

@pytest.mark.flaky(reruns=50, reruns_delay=10)
def test_new_running_batctl_wired(target, in_new_running):

    command = target.get_driver("ShellDriver")
    stdout, _, _ = command.run("batctl gwl")

    assert "No gateways in range" not in " ".join(stdout), "Batman has no gateways in range"
    assert "Error - mesh has not been enabled yet" not in " ".join(stdout), "Batman mesh is not enabled yet"
    assert "BATMAN mesh bat0 disabled - primary interface not active" not in " ".join(stdout), "Batman hat not selected an interface yet"
    assert "=>" in " ".join(stdout) or "* " in " ".join(stdout) , "no selected gw in 'batctl gwl'. command returned: {}".format(stdout)
    assert "mesh-vpn" in " ".join(stdout), "mesh-vpn was not selected by batman"

@pytest.mark.flaky(reruns=10, reruns_delay=10)
def test_new_running_internal_dns_wired(target, in_new_running, config):

    command = target.get_driver("ShellDriver")

    stdout, _, _ = command.run("ping {} -c 2".format(config["ping_target_internal_hostname"]))

    assert any(x in " ".join(stdout) for x in config["ping_target_internal_ip"]), "{} did not resolve correctly. ping returned: {}".format(config["ping_target_internal_hostname"],config["ping_target_internal_hostname"],  stdout)

def test_new_running_next_node_ip_wired(target, in_new_running, config):

    command = target.get_driver("ShellDriver")

    stdout, _, _ = command.run("ip a")

    assert config["next_node"] in " ".join(stdout), "Next-Node IP is not configured!"

@pytest.mark.flaky(reruns=5, reruns_delay=10)
def test_new_running_internal_host_reachable_wired(target, in_new_running, config):

    command = target.get_driver("ShellDriver")

    stdout, _, _ = command.run("ping {} -c 2".format(config["ping_target_internal_hostname"]))

    assert "2 packets received" in " ".join(stdout), "got packetloss to {}. ping returned: {}".format(config["ping_target_internal_hostname"], stdout)


@pytest.mark.flaky(reruns=5, reruns_delay=10)
def test_new_running_external_host_reachable_wired(target, in_new_running, config):

    command = target.get_driver("ShellDriver")

    stdout, _, _ = command.run("ping {} -c 2".format(config["ping_target_external_hostname"]))

    assert "2 packets received" in " ".join(stdout), "got packetloss to {}. ping returned: {}".format(config["ping_target_external_hostname"], stdout)

def test_new_running_enable_mesh0(target, in_new_running):
    command = target.get_driver("ShellDriver")
    stdout, _, _ = command.run("ifup mesh_radio0")

    assert len("".join(stdout)) == 0, "Could not enable mesh0 for testing of wireless uplink"

def test_new_running_disable_fastd(target, in_new_running):
    command = target.get_driver("ShellDriver")
    stdout, _, _ = command.run("ip l set mesh-vpn down")

    assert len("".join(stdout)) == 0, "Could not disable mesh-vpn for testing of wireless uplink"

    # give BATMAN time to settle
    time.sleep(20)

@pytest.mark.flaky(reruns=20, reruns_delay=10)
def test_new_running_batctl_wireless(target, in_new_running):

    command = target.get_driver("ShellDriver")
    stdout, _, _ = command.run("batctl gwl")

    assert "No gateways in range" not in " ".join(stdout), "Batman has no gateways in range"
    assert "Error - mesh has not been enabled yet" not in " ".join(stdout), "Batman mesh is not enabled yet"
    assert "BATMAN mesh bat0 disabled - primary interface not active" not in " ".join(stdout), "Batman hat not selected an interface yet"
    assert "=>" in " ".join(stdout), "no selected gw in 'batctl gwl'. command returned: {}".format(stdout)
    assert "mesh0" in " ".join(stdout), "mesh0 was not selected by batman"

@pytest.mark.flaky(reruns=5, reruns_delay=10)
def test_new_running_external_host_reachable_wireless(target, in_new_running, config):

    command = target.get_driver("ShellDriver")

    stdout, _, _ = command.run("ping {} -c 2".format(config["ping_target_external_hostname"]))

    assert "2 packets received" in " ".join(stdout), "got packetloss to {}. ping returned: {}".format(config["ping_target_external_hostname"], stdout)

def test_new_running_enable_fastd(target, in_new_running):
    command = target.get_driver("ShellDriver")
    stdout, _, _ = command.run("ip l set mesh-vpn up")

    assert len("".join(stdout)) == 0, "Could not disable mesh-vpn for testing of wireless uplink"

    # give BATMAN time to settle
    time.sleep(5)

@pytest.mark.flaky(reruns=10, reruns_delay=10)
def test_new_running_ssh(target, in_new_running, config):
    r = subprocess.run(["ssh", "-o", "UserKnownHostsFile=/dev/null", "-o", "StrictHostKeyChecking=no", "-o", "PreferredAuthentications=publickey", "-i", config["ssh_key_file"], config["ssh_connect_host_running"], "echo 1"])

    assert r.returncode == 0, "SSH connection during run mode failed"

def test_new_config_await_uhttpd_ready(target, in_new_config):
    # give it some extra time...
    time.sleep(15)

def test_new_config_correct_site(target, in_new_config):
    assert_web("http://192.168.1.1/cgi-bin/config/admin/info", "Freifunk Braunschweig")

#TODO fix version test
#def test_new_config_correct_version(target, in_new_config, fw_good):
#    fwversion = fw_good.split("-")[0]
#
#    command = target.get_driver("ShellDriver")
#    targetversion, _, _ = command.run("cat /lib/gluon/gluon-version")
#
#    assert fwversion == targetversion[0], "FW-Version on the target did not match filename of known-good after flashing."

def test_new_config_has_pubkey(target, in_new_config):
    assert_web("http://192.168.1.1/cgi-bin/config/admin/info", "69b0e6ef4242961a0b67688e0df415faae5570fec9966625b81be2c8e5025345")

def test_new_config_private_wifi_enabled(target, in_new_config):
    assert_web("http://192.168.1.1/cgi-bin/config/admin/privatewifi", 'value="1" id="id.1.1.enabled" name="id.1.1.enabled" checked="checked"')

def test_new_config_private_wifi_ssid(target, in_new_config):
    assert_web("http://192.168.1.1/cgi-bin/config/admin/privatewifi", "meinwlan")

def test_new_config_private_wifi_psk(target, in_new_config):
    assert_web("http://192.168.1.1/cgi-bin/config/admin/privatewifi", "stehtinderinfoecke")

def test_new_config_default_coordinates(target, in_new_config):
    assert_web("http://192.168.1.1/cgi-bin/config/wizard", "10.52378")
    assert_web("http://192.168.1.1/cgi-bin/config/wizard", "52.26469")

def test_new_config_altitude_field(target, in_new_config):
    assert_web("http://192.168.1.1/cgi-bin/config/wizard", "id.1.6.altitude")

def test_new_config_no_contact(target, in_new_config):
    assert_not_web("http://192.168.1.1/cgi-bin/config/wizard", "id.1.6.contact")

def test_new_config_autoupdater_enabled(target, in_new_config):
    assert_web("http://192.168.1.1/cgi-bin/config/admin/autoupdater", 'value="1" id="id.1.1.enabled" name="id.1.1.enabled" checked="checked"')

@pytest.mark.flaky(reruns=10, reruns_delay=10)
def test_new_config_ssh(target, in_new_config, config):
    r = subprocess.run(
            [
                "ssh",
                "-o",
                "UserKnownHostsFile=/dev/null",
                "-o",
                "StrictHostKeyChecking=no",
                "-o",
                "PreferredAuthentications=publickey",
                "-i",
                config["ssh_key_file"],
                config["ssh_connect_host_config"],
                "echo 1"
            ]
        )

    assert r.returncode == 0, "SSH connection during config mode failed"

