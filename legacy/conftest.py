import pytest
 
def pytest_addoption(parser):
    parser.addoption("--good", action="store", required=True)
    parser.addoption("--new", action="store", required=True)

@pytest.fixture
def fw_good(request):
    return request.config.getoption("--good")

@pytest.fixture
def fw_new(request):
    return request.config.getoption("--new")


