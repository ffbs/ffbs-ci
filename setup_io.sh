#!/bin/bash

echo 17   > /sys/class/gpio/export
echo 27   > /sys/class/gpio/export
sleep 0.1
echo high > /sys/class/gpio/gpio17/direction
echo high > /sys/class/gpio/gpio27/direction
chgrp -R dialout /sys/class/gpio/

