#! /usr/bin/env python3

from labgrid import Environment
from labgrid.driver import SmallUBootDriver, GpioDigitalOutputDriver, ShellDriver
import time
from labgrid.protocol import PowerProtocol
import os
import shutil

from labgrid import StepReporter
StepReporter()

import logging
logging.basicConfig(level=logging.DEBUG)


from labgrid.consoleloggingreporter import ConsoleLoggingReporter
ConsoleLoggingReporter.start(".")


#def run_and_print(what, command):
#    print("=========== Running '{}' =============".format(command))
#    result, _, _ = what.run(command)
#    for l in result:
#        print(l)
#
#src = os.path.join("/home/pi/", "v2018.2-ffbs-201901212225-beta-tp-link-tl-wr841n-nd-v11.bin")
#dest = os.path.join("/srv/tftp/", "good.bin")
#try:
#    if os.path.exists(dest):
#        os.remove(dest)
#    shutil.copy2(src, dest)
#except Exception as e:
#    pytest.exit("Failed to copy known-good firmware to tftp-server:"+str(e))

env = Environment("parker.yaml")

t = env.get_target("main")
#pdr = t.get_driver(PowerProtocol)
#t.activate(pdr)
st = t.get_driver("SmallUBootStrategy")
st.transition("good_config")
sh = t.get_driver("ShellDriver")

start = time.time()
for _ in range(120):
    stdout, _, _ = sh.run("ps")
    print(stdout)
    if not "/usr/bin/gluon-reconfigure" in " ".join(stdout):
        break
    time.sleep(5)
else:
    raise Exception("gluon-reconfigure took too long!")
print("gluon-reconfigure took {}s".format(time.time() - start))

#pdr.off()
#print("Device is off")
#time.sleep(1)
#
#reset = t.get_driver(GpioDigitalOutputDriver)
#print(reset.get())


#st.transition("uboot")
#print("Device reached uboot")
#
#time.sleep(5)
#st.transition("good_config")
#print("good ready")
